const { Op } = require('sequelize');
const fs = require('fs');
const path = require('path');
var json2xls = require('json2xls');

const {tbl_metafield,urlList,sequelize} = require('../models');

const sheet = async(req,res) =>{
   
    const metafields = await sequelize.query("SELECT urlList.pName, urlList.vendor_name, variantList.variant_size, variantList.variant_id, variantList.variant_name, variantList.variant_sku, variantList.variant_qty,tbl_metafield.meta_id , tbl_metafield.meta_value, variantList.status FROM variantList RIGHT JOIN urlList ON urlList.pid=variantList.pid INNER JOIN tbl_metafield ON tbl_metafield.variant_id=variantList.variant_id where variantList.variant_qty > 999 ORDER BY `urlList`.`vendor_name` ASC limit 10",
    { type: sequelize.QueryTypes.SELECT });
    updateData = metafields.map(obj => { if(obj.meta_value == null){obj.meta_value = "false";} delete obj.variant_qty;  return {...obj, status: 'No'}});
    res.send(updateData);
    var date = new Date();
    var fileName = "download/products_"+date.getFullYear()+'_'+date.getMonth()+'_'+date.getDate()+'_'+date.getSeconds()+'.xlsx';
    // var xls = json2xls(updateData);
    // fs.writeFileSync(fileName, xls, 'binary');
    console.log(fileName);
    // res.redirect('/'+fileName)
    res.xls(fileName, updateData);

}
module.exports = {
 sheet
}