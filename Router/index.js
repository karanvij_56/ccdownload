const express = require('express');
const router = express.Router();
const file =  require('../Controller/File');
router.get('/' , (req, res) => {
    res.status(201).send("API is working");
});
router.get('/sheet', file.sheet);
module.exports = router