module.exports = (sequelize, DataTypes) => {
    const variantList = sequelize.define(
      'variantList',
      {
        pid: { type: DataTypes.BIGINT },
        variant_id: { type: DataTypes.BIGINT },
        inventory_item_id: { type: DataTypes.BIGINT },
        variant_name: { type: DataTypes.STRING },
        variant_size: { type: DataTypes.BIGINT },
        variant_qty: { type: DataTypes.BIGINT },
        variant_sku: { type: DataTypes.STRING },
        status: {
          type: DataTypes.ENUM,
          values: ['Success', 'Failure']
        }
      },
      { timestamps: true, tableName: 'variantList' },
    );
  
    return variantList;
  };
  