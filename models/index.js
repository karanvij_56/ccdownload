const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const db = {};


const { host, username, password, database, port, dialect = 'mysql' } =  process.env;
var Sequelize = require('sequelize');
var sequelize = new Sequelize(database, username, password, {
  host: host,
  port: port,
  dialect: 'mysql',
  dialectOptions: { decimalNumbers: true },
  logging: false,
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.log('error');
    console.error('Unable to connect to the database:', err);
  });
  return
var dirName = __dirname;

fs.readdirSync(dirName)
  .filter((file) => {
    return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js';
  })
  .forEach((file) => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
