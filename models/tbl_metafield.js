module.exports = (sequelize, DataTypes) => {
  const tbl_metafield = sequelize.define(
    'tbl_metafield',
    {
      pid: { type: DataTypes.BIGINT },
      variant_id: { type: DataTypes.BIGINT },
      meta_id: { type: DataTypes.BIGINT },
      meta_value: { type: DataTypes.STRING },
      status: {
        type: DataTypes.ENUM,
        values: ['Success', 'Failure']
      }
    },
    { timestamps: false, tableName: 'tbl_metafield' },
  );
  tbl_metafield.associate = (models) => {
    tbl_metafield.hasOne(models.urlList, { foreignKey: 'pid' });
  };
  return tbl_metafield;
};
