module.exports = (sequelize, DataTypes) => {
    const urlList = sequelize.define(
      'urlList',
      {
        pid: { type: DataTypes.BIGINT },
        pName: { type: DataTypes.STRING },
        vendor_name: { type: DataTypes.STRING },
        product_type: { type: DataTypes.STRING },
        status: {
          type: DataTypes.ENUM,
          values: ['Success', 'Failure']
        }
      },
      { timestamps: false, tableName: 'urlList' },
    );
    urlList.associate = (models) => {
      // urlList.hasOne(models.tbl_metafield,{foreignKey: 'pid'});
      // models.Counties.belongsTo(Markets,{foreignKey: 'marketId'});
      // urlList.hasOne(models.tbl_metafield, {foreignKey: 'pid'});
      urlList.hasOne(models.tbl_metafield, { foreignKey: 'pid' });
    };
    return urlList;
  };
  