const express = require('express');
require('dotenv').config();
// const db = require('./db_connection');
var json2xls = require('json2xls');
var bodyParser = require('body-parser');
const router = require('./Router');

const app = express();
const {PORT} = process.env;
var cors = require('cors');
app.use(json2xls.middleware);

// parse application/x-www-form-urlencoded

app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())
// server your css as static
app.use(cors());
app.get('/',(req,res)=>{
    res.send('DownSheet API');
})

app.use('/api',router);
app.listen(PORT,()=>{console.log(`server run on ${PORT}`)});